package org.apache.jmeter.functions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.saxon.functions.ConstantFunction.False;


/**
 * La función debe tener la siguietne sintaxis:
 * 
 * __generaBloquesBP(bloque,paramA1;paramA2,paramA3;paramA4,paramB1;paramB2,paramB3;paramC4,...)
 * @author cesar2m
 *
 */
public class GeneraBidPrices extends AbstractFunction {
	
	private static final Logger log = LoggerFactory.getLogger(GeneraBidPrices.class);

    private static final String KEY = "__generaBloquesBP"; // $NON-NLS-1$	

    private static final List<String> desc = new LinkedList<>();
	
    private static final int NUM_PARAMETER_MIN = 2;
    private static final int NUM_MIN_VALORES_EN_BLOQUE = 4;
    
    private static final String SEPARADOR_BLOQUE_ITERABLE = "_bIterar_";
    private static final String SEPARADOR_NUM_ITERACIONES = "_nIteraciones_";
    private static final String PARAM_VAL_0 = "0?"; 
    private static final String PARAM_VAL_1 = "1?";
    private static final String PARAM_VAL_2 = "2?";
    private static final String PARAM_VAL_3 = "3?";
    
    private CompoundVariable[] values;
    
	@Override
	public String execute(SampleResult previousResult, Sampler currentSampler) 
			throws InvalidVariableException {
		
		
		
		List<String> params = new ArrayList<>();
		
		for (int i = 1; i < values.length; i++) {
			params.add(values[i].execute().trim());
			log.debug(values[i].execute().trim());
		}
		
		return crearBloques(values[0].execute().trim(),params);
		
	}

	@Override
	public void setParameters(Collection<CompoundVariable> parameters) throws InvalidVariableException {
		checkMinParameterCount(parameters, NUM_PARAMETER_MIN);
		values = parameters.toArray(new CompoundVariable[parameters.size()]);
	}
	@Override
	public List<String> getArgumentDesc() {
		return desc;
	}
	
	@Override
	public String getReferenceKey() {
		return KEY;
	}	
	
	private String crearBloques(String strPrincipal, List<String> params) {
		
		StringBuilder strR = new StringBuilder();
		
		for (String param : params) {
			
			StringBuilder sbBloque = new StringBuilder();
			String [] arrParams = param.split(";");
			
			if(faltanValores(arrParams)) {
				
				strR.append("");	//no crea bloque.
			}else {
			
				String strPrincipalConValores = strPrincipal
						.replace(PARAM_VAL_0, arrParams[0])
						.replace(PARAM_VAL_1, arrParams[1])
						.replace(PARAM_VAL_2, arrParams[2])
						.replace(PARAM_VAL_3, arrParams[3]);
				
				if(hayBloqueIterable(strPrincipal)) {
					int numIteraciones = getNumeroDeIteraciones(
							strPrincipalConValores.split(SEPARADOR_NUM_ITERACIONES));
					String strPrincipalSinSeparadorDeNumIteraciones =
							strPrincipalConValores.replace(SEPARADOR_NUM_ITERACIONES, "");
					String bloqueIterable = getBloqueIterable(strPrincipalSinSeparadorDeNumIteraciones);
					String[] bloques = strPrincipalSinSeparadorDeNumIteraciones
							.split(SEPARADOR_BLOQUE_ITERABLE);
					
					sbBloque.append(bloques[0]);
					sbBloque.append("\n");
					sbBloque.append(IteraString.iteraCadena(bloqueIterable,numIteraciones));
					
					if(bloques.length > 2) {
						sbBloque.append(bloques[2]);
						sbBloque.append("\n");
					}
					
					log.debug("Bloque: {}",sbBloque);
					
					strR.append(sbBloque);
					
	
				}else {
					strR.append(strPrincipalConValores);
					strR.append("\n");
				}	
				
				}
			
					
		}
		
		return strR.toString();
	}
	
	/**
	 * Si hay más de un valor para el número de iteracioens regresa 0 número de 
	 * iteraciones;
	 * 
	 * @param arrayConNumeroDeIteraciones
	 * @return
	 */
	private int getNumeroDeIteraciones(String ...arrayConNumeroDeIteraciones) {
	
		int numIteraciones = 0;
		String strNumIteraciones = "";
		
		if(arrayConNumeroDeIteraciones.length == 2 || arrayConNumeroDeIteraciones.length == 3) {
			strNumIteraciones = arrayConNumeroDeIteraciones[1];
		}else {
			return numIteraciones;
		}
		
		try {
			numIteraciones = Integer.parseInt(strNumIteraciones);
			return numIteraciones;
		}catch (IllegalArgumentException e) {
			return numIteraciones;
		}
	}
	
	/**
	 * El bloque iterable se espera en medio de la cadena. Está pensado para este WS en especial. 
	 * HACER que se pueda esperar en cualquier lugar.
	 * 
	 * @param strPrincipal
	 * @return
	 */
	private String getBloqueIterable(String strPrincipal) {
		
		String bloqueIterable = "";
		
		String[] bloques = strPrincipal.split(SEPARADOR_BLOQUE_ITERABLE);
		
		if(bloques.length == 2 || bloques.length == 3 ) {
			bloqueIterable = bloques[1];
			return bloqueIterable;
		}else {
			return bloqueIterable;
		}
	}
	
	/**
	 * {@link False} si hay más de un bloque Iterable.
	 * @param strPrincipal
	 * @return
	 */
	private boolean hayBloqueIterable(String strPrincipal) {
		boolean hayBloque = false;
		
		String[] bloques = strPrincipal.split(SEPARADOR_BLOQUE_ITERABLE);
		if(bloques.length == 2 || bloques.length == 3 ) {
			hayBloque = true;
		}
		return hayBloque;
	}

	/**
	 * 
	 * @param strPrincipal
	 * @return
	 */
	private List<String> getBloques(String strPrincipal){
		
		List<String> listBloques = new ArrayList<>(); 
		String[] bloques = strPrincipal.split(SEPARADOR_BLOQUE_ITERABLE);
		
		if(bloques.length > 0) {
			for (int i = 0; i < bloques.length; i++) {
				listBloques.add(bloques[i]);
			}
		}else {
			listBloques.add(strPrincipal);	
		}
		
		return listBloques;
	}
	
	/**
	 * Si algún valor viene vacío entonces regresa verdadero. 
	 * @return
	 */
	private boolean faltanValores(String[] arrParams) {
		
		if(arrParams.length <= 0 
				|| arrParams.length < NUM_MIN_VALORES_EN_BLOQUE) {
			log.debug("Tamaño de params es : {}", arrParams.length);
			return true;
		}
		
		for (int i = 0; i < arrParams.length; i++) {
			if(arrParams[i].isEmpty()) {
				log.debug("El valor de el indice {} es vacio.", i );
				return true;
			}
		}
		return false;
	}
		
	
}
