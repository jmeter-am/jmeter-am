package org.apache.jmeter.functions;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * Recibe una cadena con el valor <i>i?</i> dentro la cadena para iterar y agregar el 
 * indice en el lugar de este valor.
 *  
 * @author cesar2m
 *
 */
public class IteraString extends AbstractFunction {
    private static final Logger log = LoggerFactory.getLogger(IteraString.class);

    private static final String KEY = "__iteraCadena"; // $NON-NLS-1$	

    private static final List<String> desc = new LinkedList<>();
	
    private static final String PARAM_INDICE = "i?";
    private CompoundVariable[] values;
    
    // Número de parámetros validos.
    private static final int NUM_PARAMETER_MIN = 1;
    
	@Override
	public String execute(SampleResult previousResult, Sampler currentSampler) 
			throws InvalidVariableException {
		
		String strParaIterar = values[0].execute().trim();
		int i = Integer.parseInt(values[1].execute().trim());
		
		return iteraCadena(strParaIterar, i);
	}
	
	@Override
	public void setParameters(Collection<CompoundVariable> parameters) 
			throws InvalidVariableException {
		
		checkMinParameterCount(parameters, NUM_PARAMETER_MIN);
		values = parameters.toArray(new CompoundVariable[parameters.size()]);
	}
	
	@Override
	public List<String> getArgumentDesc() {
		return desc;
	}
	
	@Override
	public String getReferenceKey() {
		return KEY;
	}
	
	public static String iteraCadena(String strParaIterar, int i) {
		
		StringBuilder strR = new StringBuilder();
		
		for (int j = 0; j < i; j++) {
			
			strR.append(strParaIterar.replace(PARAM_INDICE, String.valueOf(j)));
			strR.append("\n");
		}
		
		return strR.toString();
	}
	
}
