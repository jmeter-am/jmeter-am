package org.apache.jmeter.functions;


import static org.junit.Assert.assertFalse;


import java.util.Collection;
import java.util.LinkedList;

import org.apache.jmeter.engine.util.CompoundVariable;

import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class GeneraBidPricesTest {

	private static final Logger LOG = LogManager.getLogger();
	
	private JMeterContext jmctx = null;
    private JMeterVariables vars = null;
    
    @Before
    public void setUp() {
    	LOG.info("GeneraBidPricesTest.setUp()");
        jmctx = JMeterContextService.getContext();
        jmctx.setVariables(new JMeterVariables());
        vars = jmctx.getVariables();
    }
	
	private Collection<CompoundVariable> collParams;
	private GeneraBidPrices generaBidPrices;
	
	@Test
	public void execute() throws InvalidVariableException {
		
		try {
			generaDatos();
			String resultado = generaBidPrices.execute(null,null);
			System.out.println("Resultado: " + resultado);
			assertFalse(resultado.isEmpty());
		}catch(Exception e) {
			LOG.error("AL crear petición: ", e.getMessage());
			e.printStackTrace();
		}
		
	}
	
//	@Before
	public void generaDatos() throws InvalidVariableException {
		
		generaBidPrices = new GeneraBidPrices();
		
		StringBuilder param1 = new StringBuilder();
		param1.append("<ns7:BidPricesVector>");
		param1.append("<ns7:CabinIndex>0?</ns7:CabinIndex>");
		param1.append("<ns7:CabinBaseClasses>1?</ns7:CabinBaseClasses>");
		param1.append("<ns7:SeatsInCabin>_nIteraciones_2?_nIteraciones_</ns7:SeatsInCabin>");
		param1.append("_bIterar_<ns7:I s=\"i?\">3?</ns7:I>_bIterar_");
		param1.append("</ns7:BidPricesVector>");
		StringBuilder param2 = new StringBuilder();
		param2.append("3").append(";");
		param2.append("Y").append(";");
		param2.append("7").append(";");
		param2.append("707");
		StringBuilder param3 = new StringBuilder();
		param3.append("2").append(";");
		param3.append("J").append(";");
		param3.append("2").append(";");
		param3.append("120");
		StringBuilder param4 = new StringBuilder();
		param4.append("1").append(";");
		param4.append("10");
		StringBuilder param5 = new StringBuilder();
		param5.append("C").append(";");
		param5.append("8").append(";");
		param5.append("88");
		
		collParams = new LinkedList<>(); 
		collParams.add(new CompoundVariable(param1.toString()));
		collParams.add(new CompoundVariable(param2.toString()));
		collParams.add(new CompoundVariable(param3.toString()));
		collParams.add(new CompoundVariable(param4.toString()));
		collParams.add(new CompoundVariable(param5.toString()));
		
		generaBidPrices.setParameters(collParams);
		
	}
	
	

}
